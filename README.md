# Updog alerts template

A template for alerts extensions for [Updog](https://git.askiiart.net/askiiart/updog). This extension does *nothing*, though, it's *just* a template.
